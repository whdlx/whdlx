// ==UserScript==
// @icon           http://baidu.com/favicon.ico
// @name           百度百科 无水印图片查看
// @namespace      http://weibo.com/liangxiafengge
// @version        1.3.1.3.2
// @description    查看百科最新版本、历史版本无水印图片，历史图册页面进入的图片暂时不支持。
// @match          http*://baike.baidu.com/picture/*
// @match          http*://baike.baidu.com/historypic/*
// @match          http*://baike.baidu.com/pic/*
// @match          http*://baike.baidu.com/picview/history/*
// @run-at         document-end
// ==/UserScript==
(function(){
    const imgPicture = document.querySelector('#imgPicture');
    //替换有水印的图片，替换“原图”中的链接
    const changeImg = ()=>{
        if (imgPicture.src.indexOf('\?') !== -1)
        {
            var imgId=window.location.href.split('pic=')[1];
            imgPicture.src='https://bkimg.cdn.bcebos.com/pic/' + imgId;
            imgPicture.url=imgPicture.src;
            document.querySelector('a.tool-button.origin').href = imgPicture.src;
        }
    };
    //首次进入需要运行一次，后续的为监测自动执行
    changeImg();
    //启动检测：          修改动作          监测对象与配置
    new MutationObserver(changeImg).observe(imgPicture, {
        attributes: true,
        childList: false,
        subtree: false
    });
})();
